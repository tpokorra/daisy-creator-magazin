#!/usr/bin/python
# -*- coding: utf-8 -*-
# pylint: disable-msg=C0103

"""
Autor: Joerg Sorge
Distributed under the terms of GNU GPL version 2 or later
Copyright (C) Joerg Sorge joergsorge at googel
2012-06-20

Dieses Programm
- kopiert mp3-Files fuer die Verarbeitung zu Daisy-Buechern
- erzeugt die noetigen Dateien fuer eine Daisy-Struktur.

This program
- makes copys of mp3-files from audiorecordings for daisy-production
- builds the daisy-struckture for digital audio books in daisy 2.02 standard

needs:
python-mutagen
lame
sudo apt-get install python-mutagen lame

update qt-GUI by development:
pyuic4 daisy_creator_mag.ui -o daisy_creator_mag_ui.py

code-checking with:
pylint --disable=W0402 --const-rgx='[a-z_A-Z][a-z0-9_]{2,30}$'
daisy_creator_mag.py
"""

import sys
import os
import shutil
import datetime
from datetime import timedelta
import ntpath
import subprocess
import string
import re
import imp
import ConfigParser
from PyQt4 import QtGui, QtCore
from mutagen.mp3 import MP3
from mutagen.id3 import ID3
from mutagen.id3 import ID3NoHeaderError

import daisy_creator_mag_ui


class DaisyCopy(QtGui.QMainWindow, daisy_creator_mag_ui.Ui_DaisyMain):
    """
    mainClass
    The second parent must be 'Ui_<obj. name of main widget class>'.
    """

    def __init__(self, parent=None):
        """Settings"""
        super(DaisyCopy, self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)
        # Pass this "self" for building widgets and
        # keeping a reference.
        self.app_debug_mod = "yes"
        self.app_bhz_items = ["Zeitschrift"]
        self.app_issue_items_prev = [
            "06", "07", "08", "09", "10", "11", "12", "21", "22", "23", "24"]
        self.app_issue_items_current = [
            "I", "II", "III", "IV", "01", "02",
            "03", "04", "05", "06", "07", "08", "09",
            "10", "11", "12", "13", "14", "15", "16",
            "17", "18", "19", "20", "21", "22", "23", "24"]
        self.app_issue_items_next = ["I", "II", "01", "02", "03"]
        self.app_bhz_path = QtCore.QDir.homePath()
        self.app_bhz_path_dest = QtCore.QDir.homePath()
        self.app_bhz_path_meta = QtCore.QDir.homePath()
        self.app_bhz_path_bhz_issue_nr = QtCore.QDir.homePath()
        self.app_bhz_path_intro = QtCore.QDir.homePath()
        self.app_bhz_path_add_file = QtCore.QDir.homePath()
        self.app_bhz_prefix_issue_nr = ""
        self.app_bhz_prefix_meta = ""
        # we need ext package lame
        self.app_lame = ""
        self.connect_actions()

    def connect_actions(self):
        """define Actions """
        self.toolButtonCopySource.clicked.connect(self.action_open_copy_source)
        self.toolButtonCopyDest.clicked.connect(self.action_open_copy_dest)
        self.toolButtonCopyFile1.clicked.connect(self.action_open_copy_file_1)
        self.toolButtonCopyFile2.clicked.connect(self.action_open_copy_file_2)
        # self.toolButtonMetaFile.clicked.connect(self.action_open_meta_file)
        self.commandLinkButton.clicked.connect(self.action_run_copy)
        self.commandLinkButtonMeta.clicked.connect(self.action_open_meta_file)
        self.commandLinkButtonDaisy.clicked.connect(self.action_run_daisy)
        self.toolButtonDaisySource.clicked.connect(
            self.action_open_daisy_source)
        self.commandLinkConfigSave.clicked.connect(self.action_save_config)
        self.pushButtonClose1.clicked.connect(self.action_quit)
        self.pushButtonClose2.clicked.connect(self.action_quit)
        self.pushButtonClose3.clicked.connect(self.action_quit)

    def read_config(self):
        """read Config from file"""
        file_exist = os.path.isfile("daisy_creator_mag.config")
        if file_exist is False:
            self.show_debug_message(u"File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Config-Datei konnte nicht geladen werden: </font>"
                + "daisy_creator_mag.config")
            return

        config = ConfigParser.RawConfigParser()
        config.read("daisy_creator_mag.config")

        try:
            # load in variables
            self.app_bhz_path = config.get('Ordner', 'BHZ')
            self.app_bhz_path_dest = config.get('Ordner', 'BHZ-Ziel')
            self.app_bhz_path_meta = config.get('Ordner', 'BHZ-Meta')
            self.app_bhz_path_bhz_issue_nr = config.get('Ordner',
                                                        'BHZ-Ausgabeansage')
            self.app_bhz_path_intro = config.get('Ordner', 'BHZ-Intro')
            self.app_bhz_path_add_file = config.get('Ordner',
                                                    'BHZ-Zusatzdatei')
            self.app_bhz_prefix_issue_nr = config.get(
                'Dateien', 'BHZ-Ausgabeansage-Praefix')
            self.app_bhz_prefix_meta = config.get(
                'Dateien', 'BHZ-Meta-Praefix')
            self.app_bhz_items = config.get(
                'Blindenhoerzeitschriften', 'BHZ').split(",")
            self.app_lame = config.get('Programme', 'LAME')

            # load in config tab entrys
            self.lineEditConfigBHZ.setText(config.get('Ordner', 'BHZ'))
            self.lineEditConfigBHZDest.setText(config.get(
                'Ordner', 'BHZ-Ziel'))
            self.lineEditConfigBHZMeta.setText(config.get(
                'Ordner', 'BHZ-Meta'))
            self.lineEditConfigBHZIssueNr.setText(config.get(
                'Ordner', 'BHZ-Ausgabeansage'))
            self.lineEditConfigBHZIntro.setText(config.get(
                'Ordner', 'BHZ-Intro'))
            self.lineEditConfigBHZFileAdditionally.setText(config.get(
                'Ordner', 'BHZ-Zusatzdatei'))
            self.lineEditConfigBHZPrefixIssueNr.setText(config.get(
                'Dateien', 'BHZ-Ausgabeansage-Praefix'))
            self.lineEditConfigBHZPrefixMeta.setText(config.get(
                'Dateien', 'BHZ-Meta-Praefix'))
            self.lineEditConfigBHZDesc.setText(config.get(
                'Blindenhoerzeitschriften', 'BHZ'))
            self.lineEditConfigLame.setText(config.get('Programme', 'LAME'))
            self.textEditConfig.append("<b>Config-Datei eingelesen:</b>")
            self.textEditConfig.append("daisy_creator_mag.config")
        except Exception, e:
            log_message = ("Fehler beim laden der Einstellungen: %s"
                           % str(e).decode('utf-8'))
            self.show_debug_message(log_message)
            self.textEditConfig.append(
                "<font color='red'>" + log_message + "</font>")
            self.show_dialog_critical(log_message)

    def read_help(self):
        """read Readme from file"""
        file_exist = os.path.isfile("README.md")
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Hilfe-Datei konnte nicht geladen werden: </font>"
                + "README.md")
            return

        fobj = open("README.md")
        for line in fobj:
            self.textEditHelp.append(line)
        # set cursor on top of helpfile
        cursor = self.textEditHelp.textCursor()
        cursor.movePosition(QtGui.QTextCursor.Start,
                            QtGui.QTextCursor.MoveAnchor, 0)
        self.textEditHelp.setTextCursor(cursor)
        fobj.close()

    def read_user_help(self):
        """read user help from file"""
        file_exist = os.path.isfile("user_help.md")
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "benutzer-Hilfe-Datei konnte nicht geladen werden: </font>"
                + "user_help.md")
            return

        fobj = open("user_help.md")
        for line in fobj:
            self.textEditUserHelp.append(line)
        # set cursor on top of helpfile
        cursor = self.textEditUserHelp.textCursor()
        cursor.movePosition(QtGui.QTextCursor.Start,
                            QtGui.QTextCursor.MoveAnchor, 0)
        self.textEditUserHelp.setTextCursor(cursor)
        fobj.close()

    def action_save_config(self):
        """save config file"""
        display_message = "Config- und Metadateien mit neuen Werten speichern!"
        action = self.show_dialog_question(display_message)
        if action == 1:
            self.backup_config_file()
            self.write_config_file()
            self.make_config_dirs()
            self.copy_config_meta_files()
            self.copy_config_bhz_intro_files()
            # load after processing the other stuff
            self.read_config()
            self.progressBarConfig.setValue(60)
            self.change_meta_year()
            # self.copy_config_bhz_issue_nr_files()

    def action_open_copy_source(self):
        """Source of copy"""
        # QtCore.QDir.homePath()
        dir_source = QtGui.QFileDialog.getExistingDirectory(
            self, "Quell-Ordner", self.app_bhz_path)
        # Don't attempt to open if open dialog
        # was cancelled away.
        if dir_source:
            self.lineEditCopySource.setText(dir_source)
            self.textEdit.append("Quelle:")
            self.textEdit.append(dir_source)

    def action_open_daisy_source(self):
        """Source of daisy"""
        dir_source = QtGui.QFileDialog.getExistingDirectory(
            self, "Quell-Ordner", self.app_bhz_path_dest)
        # Don't attempt to open if open dialog
        # was cancelled away.
        if dir_source:
            self.lineEditDaisySource.setText(dir_source)
            self.textEdit.append("Quelle:")
            self.textEdit.append(dir_source)

    def action_open_copy_dest(self):
        """Destination for Copy"""
        dir_dest = QtGui.QFileDialog.getExistingDirectory(
            self, "Ziel-Ordner", self.app_bhz_path_dest)
        # Don't attempt to open if open dialog
        # was cancelled away.
        if dir_dest:
            self.lineEditCopyDest.setText(dir_dest)
            self.textEdit.append("Ziel:")
            self.textEdit.append(dir_dest)

    def action_open_copy_file_1(self):
        """Additional file 1 to copy"""
        file_1 = QtGui.QFileDialog.getOpenFileName(
            self, "Datei 1", self.app_bhz_path_add_file)
        # Don't attempt to open if open dialog
        # was cancelled away.
        if file_1:
            self.lineEditCopyFile1.setText(file_1)
            self.textEdit.append("Zusatz-Datei 1:")
            self.textEdit.append(file_1)
            check_ok = self.check_filename(file_1)
            if check_ok is None:
                return

    def action_open_copy_file_2(self):
        """Additional file 2 to copy"""
        file_2 = QtGui.QFileDialog.getOpenFileName(
            self, "Datei 2", self.app_bhz_path_add_file)
        # Don't attempt to open if open dialog
        # was cancelled away.
        if file_2:
            self.lineEditCopyFile2.setText(file_2)
            self.textEdit.append("Zusatz-Datei 2:")
            self.textEdit.append(file_2)
            check_ok = self.check_filename(file_2)
            if check_ok is None:
                return

    def action_open_meta_file(self):
        """Metafile to load"""
        meta_file = QtGui.QFileDialog.getOpenFileName(
            self,
            "Daisy_Meta",
            self.app_bhz_path_meta)
        # Don't attempt to open if open dialog
        # was cancelled away.
        if meta_file:
            self.lineEditMetaSource.setText(meta_file)
            self.read_meta_file(str(meta_file))

    def action_run_copy(self):
        """Mainfunction to copy"""
        if self.lineEditCopySource.text() == "Quell-Ordner":
            error_message = "Quell-Ordner wurde nicht ausgewaehlt.."
            self.show_dialog_critical(error_message)
            return

        if self.lineEditCopyDest.text() == "Ziel-Ordner":
            error_message = "Ziel-Ordner wurde nicht ausgewaehlt.."
            self.show_dialog_critical(error_message)
            return

        self.show_debug_message(self.lineEditCopySource.text())
        self.show_debug_message(self.lineEditCopyDest.text())

        # check for files in source
        try:
            dir_source = os.listdir(self.lineEditCopySource.text())
        except Exception, e:
            error_message = "Quelle: %s" % str(e)
            self.show_debug_message(error_message)
            self.show_dialog_critical(error_message)
            return

        # ceck dir of dest
        if os.path.exists(self.lineEditCopyDest.text()) is False:
            error_message = "Ziel-Ordner existiert nicht.."
            self.show_debug_message(error_message)
            self.show_dialog_critical(error_message)
            self.lineEditCopyDest.setFocus()
            return

        self.show_debug_message(dir_source)

        # check for mp3 files, white spaces or special characters in file names
        self.textEdit.append("<b>Pruefen...</b>")
        check_ok = self.check_filenames(dir_source)
        if check_ok is None:
            return

        n_count_mp3_files = self.count_mp3_files(dir_source)

        self.textEdit.append("<b>Kopieren:</b>")
        n_counter = 0
        # n_list = len(dir_source)
        # n_list = n_count_mp3_files
        self.show_debug_message(n_count_mp3_files)
        dir_source.sort()
        for item in dir_source:
            if (item[len(item) - 4:len(item)] != ".MP3"
                    and item[len(item) - 4:len(item)] != ".mp3"):
                continue

            file_to_copy_source = self.lineEditCopySource.text() + "/" + item
            # check if file exist
            file_exist = os.path.isfile(file_to_copy_source)
            if file_exist is False:
                self.show_debug_message("File not exists")
                # change max number and update progress
                n_count_mp3_files = n_count_mp3_files - 1
                n_count_progress = n_counter * 100 / n_count_mp3_files
                self.progressBarCopy.setValue(n_count_progress)
                self.textEdit.append(
                    "<b>Datei konnte nicht kopiert werden: </b>"
                    + file_to_copy_source)
                self.show_debug_message(file_to_copy_source)
                self.textEdit.append("<b>Uebersprungen</b>:")
                self.textEdit.append(file_to_copy_source)
                continue

            # cange filenames
            if self.checkBoxDaisyIgnoreTitleDigits.isChecked():
                if self.checkBoxDaisyLevel.isChecked():
                    file_to_copy_dest = (
                        item[0:6] + "_"
                        + self.comboBoxCopyBhz.currentText()
                        + "_" + self.comboBoxCopyBhzAusg.currentText()
                        + "_" + item[6:len(item) - 4] + ".mp3")
                else:
                    file_to_copy_dest = (
                        item[0:4] + "_"
                        + self.comboBoxCopyBhz.currentText()
                        + "_" + self.comboBoxCopyBhzAusg.currentText()
                        + "_" + item[5:len(item) - 4] + ".mp3")
            else:
                file_to_copy_dest = (
                    item[0:len(item) - 4] + "_"
                    + self.comboBoxCopyBhz.currentText()
                    + "_" + self.comboBoxCopyBhzAusg.currentText()
                    + "_" + item[0:len(item) - 4] + ".mp3")

            if self.checkBoxCopyChangeNr1001.isChecked():
                # rename 1001.mp3 in 0100 umbenennen
                # to further sort front
                if item[0:4] == "1001":
                    file_to_copy_dest = (
                        "/0100_" + self.comboBoxCopyBhz.currentText()
                        + "_" + self.comboBoxCopyBhzAusg.currentText()
                        + "_" + item[0:len(item) - 4]
                        + ".mp3")
                    self.textEdit.append(
                        "<b>1000.mp3 in 0100 umbenannt "
                        "damit die Ansage weiter vorn einsortiert wird</b>")

            if self.checkBoxCopyChangeNr1000.isChecked():
                # rename 1000.mp3 in 0100 umbenennen
                # to further sort front
                if item[0:4] == "1000":
                    file_to_copy_dest = (
                        "/0100_" + self.comboBoxCopyBhz.currentText()
                        + "_" + self.comboBoxCopyBhzAusg.currentText()
                        + "_" + item[0:len(item) - 4] + ".mp3")
                    self.textEdit.append(
                        "<b>1000.mp3 in 0100 umbenannt "
                        "damit die Ansage weiter vorn einsortiert wird</b>")

            if self.checkBoxCopyFileNameShort.isChecked():
                # shorten dest file name to 60 characters
                file_to_copy_dest = self.check_filename_lenght(
                    file_to_copy_dest)

            # complete with path
            file_to_copy_dest = (self.lineEditCopyDest.text()
                                 + "/" + str(file_to_copy_dest))

            self.textEdit.append(file_to_copy_dest)
            self.show_debug_message(file_to_copy_source)
            self.show_debug_message(file_to_copy_dest)

            # check bitrate, when necessary recode in new destination
            is_changed_and_copy = self.check_change_bitrate_and_copy(
                file_to_copy_source, file_to_copy_dest)
            # nothing to do, only copy
            # isChangedAndCopy = None
            if is_changed_and_copy is None:
                self.copy_file(file_to_copy_source, file_to_copy_dest)

            self.check_cange_id3(file_to_copy_dest)
            n_counter += 1
            self.show_debug_message(n_counter)
            self.show_debug_message(n_count_mp3_files)
            n_count_progress = n_counter * 100 / n_count_mp3_files
            self.show_debug_message(n_count_progress)
            self.progressBarCopy.setValue(n_count_progress)

        self.show_debug_message(n_counter)
        if self.checkBoxCopyBhzIntro.isChecked():
            self.copy_intro()

        if self.checkBoxCopyBhzAusgAnsage.isChecked():
            self.copy_bhz_issue_nr()

        if self.lineEditCopyFile1.text() != "Datei 1 waehlen":
            self.copy_file_additionally(1)

        if self.lineEditCopyFile2.text() != "Datei 2 waehlen":
            self.copy_file_additionally(2)

        # load metadata
        self.lineEditMetaSource.setText(
            self.app_bhz_path_meta + "/" + self.app_bhz_prefix_meta + "_"
            + str(self.comboBoxCopyBhz.currentText()))
        self.read_meta_file(str(self.lineEditMetaSource.text()))
        # enter path of source and destination
        self.lineEditDaisySource.setText(self.lineEditCopyDest.text())

    def backup_config_file(self):
        """backup config file """
        try:
            shutil.copy(
                "daisy_creator_mag.config", "daisy_creator_mag.config.bak")
            self.show_debug_message("backup config file")
            self.textEditConfig.append("<b>Config-Datei gesichert:</b>")
            self.textEditConfig.append("daisy_creator_mag.config.bak")
            self.progressBarConfig.setValue(10)
        except Exception, e:
            log_message = "copy_file Error: %s" % str(e).decode('utf-8')
            self.show_debug_message(log_message)
            self.textEditConfig.append(
                "<font color='red'>" + log_message
                + "daisy_creator_mag.config</font>")

    def change_meta_year(self):
        """change meta year"""
        self.textEditConfig.append("<b>Meta-Datei angepasst:</b>")
        try:
            files_source = os.listdir(str(self.lineEditConfigBHZMeta.text()))
            if not files_source:
                return

            for item in files_source:
                # filter out backup files
                if item[len(item) - 1:len(item)] == "~":
                    continue
                path_file_source = (str(self.lineEditConfigBHZMeta.text())
                                    + "/" + item)
                print path_file_source
                self.read_meta_file(path_file_source)
                self.write_meta_file(path_file_source)
                self.textEditConfig.append(item)
            self.progressBarConfig.setValue(100)
        except Exception, e:
            log_message = "change meta file Error: %s" % str(e).decode('utf-8')
            self.show_debug_message(log_message)
            self.textEditConfig.append(
                "<font color='red'>" + log_message + "</font>")

    def copy_file(self, file_to_copy_source, file_to_copy_dest):
        """copy file"""
        try:
            shutil.copy(file_to_copy_source, file_to_copy_dest)
        except Exception, e:
            log_message = "copy_file Error: %s" % str(e).decode('utf-8')
            self.show_debug_message(log_message)
            self.textEdit.append(
                "<font color='red'>" + log_message
                + file_to_copy_dest + "</font>")

    def copy_config_meta_files(self):
        """copy config meta files"""
        self.textEditConfig.append("<b>Meta-Dateien kopieren:</b>")

        files_source = os.listdir(self.app_bhz_path_meta)
        if not files_source:
            return

        for item in files_source:
            # filter out backup files
            if item[len(item) - 1:len(item)] == "~":
                continue
            path_file_source = self.app_bhz_path_meta + "/" + item
            path_file_dest = (str(self.lineEditConfigBHZMeta.text())
                              + "/" + item)
            try:
                shutil.copy(path_file_source, path_file_dest)
            except Exception, e:
                log_message = "copy_file Error: %s" % str(e).decode('utf-8')
                self.show_debug_message(log_message)
                self.textEditConfig.append(
                    "<font color='red'>" + log_message + "</font>")
            self.textEditConfig.append(item)

        self.progressBarConfig.setValue(40)

    def copy_config_bhz_issue_nr_files(self):
        """copy config bhz issue nr files"""
        self.textEditConfig.append("Intro-Dateien kopieren:")

        files_source = os.listdir(self.app_bhz_path_bhz_issue_nr)
        if not files_source:
            return

        for item in files_source:
            # filter out backup files
            if item[len(item) - 1:len(item)] == "~":
                continue
            path_file_source = self.app_bhz_path_bhz_issue_nr + "/" + item
            # print path_file_source
            path_file_dest = (str(self.lineEditConfigBHZIssueNr.text())
                              + "/" + item)
            try:
                shutil.copy(path_file_source, path_file_dest)
            except Exception, e:
                log_message = "copy_file Error: %s" % str(e).decode('utf-8')
                self.show_debug_message(log_message)
                self.textEditConfig.append(
                    "<font color='red'>" + log_message + "</font>")
            self.textEditConfig.append(item)

    def copy_config_bhz_intro_files(self):
        """copy config bhz intro files"""
        self.textEditConfig.append("<b>Intro-Dateien kopieren:</b>")

        files_source = os.listdir(self.app_bhz_path_intro)
        # print files_source
        if not files_source:
            return

        for item in files_source:
            # filter out backup files
            if item[len(item) - 1:len(item)] == "~":
                continue
            # filter out non mp3 files
            if (item[len(item) - 4:len(item)] != ".MP3" and
                    item[len(item) - 4:len(item)] != ".mp3"):
                continue

            path_file_source = self.app_bhz_path_intro + "/" + item
            # print path_file_source
            path_file_dest = (str(self.lineEditConfigBHZIntro.text())
                              + "/" + item)
            try:
                shutil.copy(path_file_source, path_file_dest)
            except Exception, e:
                log_message = "copy_file Error: %s" % str(e).decode('utf-8')
                self.show_debug_message(log_message)
                self.textEditConfig.append(
                    "<font color='red'>" + log_message + "</font>")
            self.textEditConfig.append(item)

        self.progressBarConfig.setValue(50)

    def copy_intro(self):
        """copy Intro"""
        file_to_copy_source = (self.app_bhz_path_intro + "/Intro_"
                               + self.comboBoxCopyBhz.currentText() + ".mp3")
        # if self.comboBoxCopyBhz.currentText() == "Bibel_fuer_heute":
        if self.checkBoxDaisyLevel.isChecked():
            # include level in filename
            file_to_copy_dest = (self.lineEditCopyDest.text() + "/0010_1_"
                                 + self.comboBoxCopyBhz.currentText() + "_"
                                 + self.comboBoxCopyBhzAusg.currentText()
                                 + "_Intro.mp3")
        else:
            file_to_copy_dest = (self.lineEditCopyDest.text() + "/0010_"
                                 + self.comboBoxCopyBhz.currentText() + "_"
                                 + self.comboBoxCopyBhzAusg.currentText()
                                 + "_Intro.mp3")
        self.show_debug_message(file_to_copy_source)
        self.show_debug_message(file_to_copy_dest)

        file_exist = os.path.isfile(file_to_copy_source)
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Intro nicht vorhanden</font>: "
                + os.path.basename(str(file_to_copy_source)))
            return

        self.copy_file(file_to_copy_source, file_to_copy_dest)
        self.check_cange_id3(file_to_copy_dest)

    def copy_bhz_issue_nr(self):
        """copy issue number"""
        path_issue = (self.app_bhz_path_bhz_issue_nr
                      + "/" + self.app_bhz_prefix_issue_nr
                      + "_" + self.comboBoxCopyBhz.currentText())
        self.show_debug_message(path_issue)
        file_to_copy_source = (
            path_issue + "/0001_"
            + self.comboBoxCopyBhz.currentText() + "_"
            + self.comboBoxCopyBhzAusg.currentText() + "_Ausgabe.mp3")
        file_to_copy_dest = (
            self.lineEditCopyDest.text() + "/0001_"
            + self.comboBoxCopyBhz.currentText() + "_"
            + self.comboBoxCopyBhzAusg.currentText() + "_Ausgabe.mp3")
        self.show_debug_message(file_to_copy_source)
        self.show_debug_message(file_to_copy_dest)

        file_exist = os.path.isfile(file_to_copy_source)
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Ausgabeansage nicht vorhanden</font>: "
                + os.path.basename(str(file_to_copy_source)))
            return

        self.copy_file(file_to_copy_source, file_to_copy_dest)
        self.check_cange_id3(file_to_copy_dest)

    def copy_file_additionally(self, n):
        """copy additional file"""
        if n == 1:
            filename = ntpath.basename(str(self.lineEditCopyFile1.text()))
            patFilenameSource = str(self.lineEditCopyFile1.text())

        if n == 2:
            filename = ntpath.basename(str(self.lineEditCopyFile2.text()))
            patFilenameSource = str(self.lineEditCopyFile2.text())

        self.show_debug_message(u"Bitrate check: " + filename)

        if self.checkBoxCopyFileNameShort.isChecked():
            # shorten dest file name to 60 characters
            filename_checked = self.check_filename_lenght(filename)

        file_to_copy_dest = (str(self.lineEditCopyDest.text())
                             + "/" + filename_checked)

        # check bitrate, when necessary recode in new destination
        is_changed_and_copy = self.check_change_bitrate_and_copy(
            patFilenameSource, str(file_to_copy_dest))
        # nothing to do, only copy
        if is_changed_and_copy is None:
            # shutil.copy( file_to_copy_source, fileToCopyDest )
            self.copy_file(patFilenameSource, file_to_copy_dest)
            self.check_cange_id3(file_to_copy_dest)

    def check_packages(self, package):
        """
        check if package is installed
        needs subprocess, os
        http://stackoverflow.com/
        questions/11210104/check-if-a-program-exists-from-a-python-script
        """
        try:
            devnull = open(os.devnull, "w")
            subprocess.Popen([package], stdout=devnull,
                             stderr=devnull).communicate()
        except OSError as e:
            if e.errno == os.errno.ENOENT:
                error_message = (
                    u"Es fehlt das Paket:\n " + package
                    + u"\nZur Nutzung des vollen Funktionsumfanges "
                    + "muss es installiert werden!")
                self.show_dialog_critical(error_message)
                self.textEdit.append(
                    "<font color='red'>Es fehlt das Paket: </font> " + package)

    def check_modules(self, pModule):
        """
        check if python-module is installed
        needs imp
        """
        try:
            imp.find_module(pModule)
            return True
        except ImportError:
            error_message = (u"Es fehlt das Python-Modul:\n " + pModule
                             + u"\nZur Nutzung des vollen Funktionsumfanges "
                             + "muss es installiert werden!")
            self.show_dialog_critical(error_message)
            self.textEdit.append(
                "<font color='red'>Es fehlt das Python-Modul: </font> "
                + pModule)
            return False

    def check_filename(self, file_name):
        """check for spaces and non ascii characters"""
        error = None
        self.textEdit.append("Dateiname pruefen:")
        self.textEdit.append(file_name)
        if type(file_name) is str:
            try:
                c_file_name = file_name.encode("ascii")
            except Exception, e:
                error = str(e)
        else:
            # maybe file_name could be QString, so we must convert it
            try:
                c_file_name = str(file_name)
                # self.textEdit.append(c_file_name)
            except Exception, e:
                error = str(e)

        if error is not None:
            if (error.find("'ascii' codec can't encode character") != -1 or
                    error.find("'ascii' codec can't decode byte") != -1):
                error_message = (
                    "<b>Unerlaubte(s) Zeichen im Dateinamen!</b>")
                self.show_debug_message(error_message)
                self.textEdit.append(error_message)
                self.tabWidget.setCurrentIndex(1)
                return None
            else:
                error_message = ("<b>Fehler im Dateinamen!</b>")
                self.show_debug_message(error_message)
                self.textEdit.append(error_message)
                self.tabWidget.setCurrentIndex(1)
                return None

        if c_file_name.find(" ") != -1:
            error_message = ("<b>Unerlaubtes Leerzeichen im Dateinamen!</b>")
            self.textEdit.append(error_message)
            self.tabWidget.setCurrentIndex(1)
            return None
        return "OK"

    def check_filenames(self, files_source):
        """check for mp3 files"""
        for item in files_source:
            if (item[len(item) - 4:len(item)] != ".MP3" and
                    item[len(item) - 4:len(item)] != ".mp3"):
                continue
            check_ok = self.check_filename(item)
            if check_ok is None:
                return None
        return "OK"

    def check_filename_lenght(self, filename_to_check):
        """check lengt of filename, and shortening if necessary"""
        if len(filename_to_check) > 60:
            # base name without ext.
            file_name_temp = filename_to_check[0:len(filename_to_check) - 4]
            file_name_changed = file_name_temp[0:56] + ".mp3"
            self.textEdit.append(
                "<font color='blue'>"
                + "Dateiname gekuerzt:</font><br> "
                + file_name_changed)
            return file_name_changed
        else:
            return filename_to_check

    def count_mp3_files(self, files_source):
        """count mp3 files"""
        z = 0
        for item in files_source:
            if (item[len(item) - 4:len(item)] != ".MP3" and
                    item[len(item) - 4:len(item)] != ".mp3"):
                continue
            else:
                z += 1
        return z

    def check_cange_id3(self, file_to_copy_dest):
        """check id3 Tags, mayby kill it"""
        tag = None
        try:
            audio = ID3(str(file_to_copy_dest))
            tag = "yes"
        except ID3NoHeaderError:
            self.show_debug_message("No ID3 header found; skipping.")

        if tag is not None:
            if self.checkBoxCopyID3Change.isChecked():
                audio.delete()
                self.textEdit.append("<b>ID3 entfernt bei</b>: "
                                     + file_to_copy_dest)
                self.show_debug_message(
                    "ID3 entfernt bei " + file_to_copy_dest)
            else:
                self.textEdit.append(
                    "<b>ID3 vorhanden, aber NICHT entfernt bei</b>: "
                    + file_to_copy_dest)

    def check_change_bitrate_and_copy(self, file_to_copy_source, file_to_copy_dest):
        """check bitrate, when necessary recode in new destination"""
        is_changed_and_copy = None
        audio_source = MP3(str(file_to_copy_source))
        if (audio_source.info.bitrate / 1000 ==
                int(self.comboBoxPrefBitrate.currentText())):
            return is_changed_and_copy

        is_encoded = None
        self.textEdit.append("Bitrate Vorgabe: "
                             + str(self.comboBoxPrefBitrate.currentText()))
        self.textEdit.append(
            u"<b>Bitrate folgender Datei entspricht nicht der Vorgabe:</b> "
            + str(audio_source.info.bitrate / 1000) + " " + file_to_copy_source)

        if self.checkBoxCopyBitrateChange.isChecked():
            self.textEdit.append(u"<b>Bitrate aendern bei</b>: "
                                 + file_to_copy_dest)
            is_encoded = self.encode_file(
                file_to_copy_source, file_to_copy_dest)
            if is_encoded is not None:
                self.textEdit.append(u"<b>Bitrate geaendert bei</b>: "
                                     + file_to_copy_dest)
                is_changed_and_copy = True
        else:
            self.textEdit.append(u"<b>Bitrate wurde NICHT geaendern bei</b>: "
                                 + file_to_copy_dest)
        return is_changed_and_copy

    def encode_file(self, file_to_copy_source, file_to_copy_dest):
        """encode mp3-file """
        self.show_debug_message(u"encode_file")
        # characterset of commands is importand
        # encoding in the right manner
        # c_lame_encoder = "/usr/bin/lame"
        # self.show_debug_message(u"type c_lame_encoder")
        # self.show_debug_message(type(c_lame_encoder))
        self.show_debug_message(u"file_to_copy_source")
        self.show_debug_message(type(file_to_copy_source))
        self.show_debug_message(file_to_copy_dest)
        self.show_debug_message(u"type(file_to_copy_dest)")
        self.show_debug_message(type(file_to_copy_dest))

        try:
            p = subprocess.Popen(
                [self.app_lame, "-b",
                 self.comboBoxPrefBitrate.currentText(),
                 "-m", "m",
                 file_to_copy_source, file_to_copy_dest],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE).communicate()

            self.show_debug_message(u"returncode 0")
            self.show_debug_message(p[0])
            self.show_debug_message(u"returncode 1")
            self.show_debug_message(p[1])

            # search for success-message, when not found : -1
            n_encode_percent = string.find(p[1], "(100%)")
            n_encode_percent_1 = string.find(p[1], "(99%)")
            self.show_debug_message(n_encode_percent)
            c_complete = "no"

            # if the file is very short, no 100% message appear,
            # then also accept 99%
            if n_encode_percent == -1:
                # no 100%
                if n_encode_percent_1 != -1:
                    #  but 99
                    c_complete = "yes"
            else:
                c_complete = "yes"

            if c_complete == "yes":
                log_message = "recoded_file: " + file_to_copy_source
                self.show_debug_message(log_message)
                return file_to_copy_dest
            else:
                log_message = "recode_file Error: " + file_to_copy_source
                self.show_debug_message(log_message)
                return None

        except Exception, e:
            errMessage = "Fehler beim Encodieren: %s" % str(e)
            self.show_debug_message(errMessage)
            self.show_dialog_critical(errMessage)
            self.textEdit.append(
                "<font color='red'>Fehler beim Encodieren:</font> "
                + os.path.basename(str(file_to_copy_source)))

    def read_meta_file(self, meta_file):
        """load file with meta-data"""
        # print "read meta"
        # print meta_file
        if meta_file is None:
            meta_file = (str(self.lineEditMetaSource.text())
                         + "/" + self.app_bhz_prefix_meta)
            # print "is none"

        file_exist = os.path.isfile(meta_file)
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Meta-Datei konnte nicht geladen werden</font>: "
                + os.path.basename(meta_file))
            return

        # print "hio"
        # print meta_file
        config = ConfigParser.RawConfigParser()
        config.read(meta_file)
        try:
            self.lineEditMetaProducer.setText(
                config.get('Daisy_Meta', 'Produzent'))
            self.lineEditMetaAutor.setText(config.get('Daisy_Meta', 'Autor'))
            self.lineEditMetaTitle.setText(config.get('Daisy_Meta', 'Titel'))
            self.lineEditMetaEdition.setText(
                config.get('Daisy_Meta', 'Edition'))
            self.lineEditMetaNarrator.setText(
                config.get('Daisy_Meta', 'Sprecher'))
            self.lineEditMetaKeywords.setText(
                config.get('Daisy_Meta', 'Stichworte'))
            self.lineEditMetaRefOrig.setText(
                config.get('Daisy_Meta', 'ISBN/Ref-Nr.Original'))
            self.lineEditMetaPublisher.setText(
                config.get('Daisy_Meta', 'Verlag'))
            self.lineEditMetaYear.setText(config.get('Daisy_Meta', 'Jahr'))
        except Exception, e:
            log_message = ("Fehler beim laden der Meta-Daten: %s"
                           % str(e).decode('utf-8'))
            self.show_debug_message(log_message)
            self.textEditConfig.append(
                "<font color='red'>" + log_message + "</font>")
            self.show_dialog_critical(log_message)

    def action_run_daisy(self):
        """create daisy-fileset"""
        if self.lineEditDaisySource.text() == "Quell-Ordner":
            error_message = "Quell-Ordner wurde nicht ausgewaehlt.."
            self.show_dialog_critical(error_message)
            return

        # read audiofiles
        try:
            dir_items = os.listdir(self.lineEditDaisySource.text())
        except Exception, e:
            log_message = u"read_files_from_dir Error: %s" % str(e)
            self.show_debug_message(log_message)

        self.progressBarDaisy.setValue(10)
        self.show_debug_message(dir_items)
        self.textEditDaisy.append(u"<b>Folgende Audios werden bearbeitet:</b>")
        n_mp3 = 0
        n_list = len(dir_items)
        self.show_debug_message(n_list)
        dir_audios = []
        dir_items.sort()
        for item in dir_items:
            if (item[len(item) - 4:len(item)] == ".MP3"
                    or item[len(item) - 4:len(item)] == ".mp3"):
                dir_audios.append(item)
                self.textEditDaisy.append(item)
                n_mp3 += 1

        # n_total_audio_length = self.calc_audio_lengt(dir_audios)
        l_times = self.calc_audio_lengt(dir_audios)
        n_total_audio_length = l_times[0]
        lTotalElapsedTime = l_times[1]
        lFileTime = l_times[2]
        # print n_total_audio_length
        totalTime = timedelta(seconds=n_total_audio_length)
        # change from timedelta in to string
        # hours, minits and seconds must have 2 digits (zfill(8))

        lTotalTime = str(totalTime).split(".")
        cTotalTime = lTotalTime[0].zfill(8)
        # str(cTotalTime[0]).zfill(8)
        self.textEditDaisy.append("Gesamtlaenge: " + cTotalTime)
        self.write_NCC(cTotalTime, n_mp3, dir_audios)
        self.progressBarDaisy.setValue(20)
        self.write_master_smil(cTotalTime, dir_audios)
        self.progressBarDaisy.setValue(50)
        self.write_smil(lTotalElapsedTime, lFileTime, dir_audios)
        self.progressBarDaisy.setValue(100)

    def calc_audio_lengt(self, dir_audios):
        """calc total length"""
        n_total_audio_length = 0
        lTotalElapsedTime = []
        lTotalElapsedTime.append(0)
        lFileTime = []
        for item in dir_audios:
            fileToCheck = os.path.join(
                str(self.lineEditDaisySource.text()), item)
            audio_source = MP3(fileToCheck)
            self.show_debug_message(item + " " + str(audio_source.info.length))
            n_total_audio_length += audio_source.info.length
            lTotalElapsedTime.append(n_total_audio_length)
            lFileTime.append(audio_source.info.length)
            l_times = []
            l_times.append(n_total_audio_length)
            l_times.append(lTotalElapsedTime)
            l_times.append(lFileTime)
        return l_times

    def write_NCC(self, cTotalTime, n_mp3, dir_audios):
        """write NCC-Page"""
        # Levels
        max_level = "1"
        # find max-level
        if self.checkBoxDaisyLevel.isChecked():
            for item in dir_audios:
                self.show_debug_message("Level: " + item[5:6])
                if re.match("\d{1,}", item[5:6]) is not None:
                    if item[5:6] > max_level:
                        max_level = item[5:6]

        try:
            f_out_file = open(
                os.path.join(
                    str(self.lineEditDaisySource.text()), "ncc.html"), 'w')
        except IOError as (errno, strerror):
            self.show_debug_message(
                "I/O error({0}): {1}".format(errno, strerror))
            return
        # else:
        self.textEditDaisy.append(u"<b>NCC-Datei schreiben...</b>")
        f_out_file.write('<?xml version="1.0" encoding="utf-8"?>' + '\r\n')
        f_out_file.write(
            '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0'
            + ' Transitional//EN"'
            + ' "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'
            + '\r\n')
        f_out_file.write('<html xmlns="http://www.w3.org/1999/xhtml">'
                         + '\r\n')
        f_out_file.write('<head>' + '\r\n')
        f_out_file.write('<meta http-equiv="Content-type" '
                         + 'content="text/html; charset=utf-8"/>' + '\r\n')
        f_out_file.write('<title>' + self.comboBoxCopyBhz.currentText()
                         + '</title>' + '\r\n')

        f_out_file.write('<meta name="ncc:generator" '
                         + 'content="KOM-IN-DaisyCreator"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:revision" content="1"/>' + '\r\n')
        today = datetime.date.today()
        f_out_file.write('<meta name="ncc:producedDate" content="'
                         + today.strftime("%Y-%m-%d") + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:revisionDate" content="'
                         + today.strftime("%Y-%m-%d") + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:tocItems" content="'
                         + str(n_mp3) + '"/>' + '\r\n')

        f_out_file.write('<meta name="ncc:totalTime" content="'
                         + cTotalTime + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:narrator" content="'
                         + self.lineEditMetaNarrator.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:pageNormal" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:pageFront" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:pageSpecial" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:sidebars" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:prodNotes" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:footnotes" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:depth" content="' + max_level + '"/>'
                         + '\r\n')
        f_out_file.write('<meta name="ncc:maxPageNormal" content="0"/>'
                         + '\r\n')
        f_out_file.write('<meta name="ncc:charset" content="utf-8"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:multimediaType" content="audioNcc"/>'
                         + '\r\n')
        # f_out_file.write( '<meta name="ncc:kByteSize" content=" "/>'+ '\r\n')
        f_out_file.write('<meta name="ncc:setInfo" content="1 of 1"/>'
                         + '\r\n')
        f_out_file.write('<meta name="ncc:sourceDate" content="'
                         + self.lineEditMetaYear.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:sourceEdition" content="'
                         + self.lineEditMetaEdition.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:sourcePublisher" content="'
                         + self.lineEditMetaPublisher.text() + '"/>' + '\r\n')

        # Anzahl files = Records 2x + ncc.html + master.smil
        f_out_file.write('<meta name="ncc:files" content="'
                         + str(n_mp3 + n_mp3 + 2) + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:producer" content="'
                         + self.lineEditMetaProducer.text() + '"/>' + '\r\n')

        f_out_file.write('<meta name="dc:creator" content="'
                         + self.lineEditMetaAutor.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="dc:date" content="'
                         + today.strftime("%Y-%m-%d") + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:format" content="Daisy 2.02"/>' + '\r\n')
        f_out_file.write('<meta name="dc:identifier" content="'
                         + self.lineEditMetaRefOrig.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="dc:language" content="de"'
                         + ' scheme="ISO 639"/>' + '\r\n')
        f_out_file.write('<meta name="dc:publisher" content="'
                         + self.lineEditMetaPublisher.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="dc:source" content="'
                         + self.lineEditMetaRefOrig.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="dc:subject" content="'
                         + self.lineEditMetaKeywords.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="dc:title" content="'
                         + self.lineEditMetaTitle.text() + '"/>' + '\r\n')
        # Medibus-OK items
        f_out_file.write(
            '<meta name="prod:audioformat" content="wave 44 kHz"/>' + '\r\n')
        f_out_file.write(
            '<meta name="prod:compression" content="mp3 '
            + self.comboBoxPrefBitrate.currentText() + '/ kb/s"/>' + '\r\n')
        f_out_file.write('<meta name="prod:localID" content=" "/>' + '\r\n')
        f_out_file.write('</head>' + '\r\n')
        f_out_file.write('<body>' + '\r\n')
        n_counter = 0
        for item in dir_audios:
            n_counter += 1
            if n_counter == 1:
                f_out_file.write(
                    '<h1 class="title" id="cnt_0001">'
                    + '<a href="0001.smil#txt_0001">'
                    + self.lineEditMetaAutor.text() + ": "
                    + self.lineEditMetaTitle.text()
                    + '</a></h1>' + '\r\n')
                continue

            # extract title
            self.show_debug_message("extract title...")
            item_splitted = self.split_filename(item)
            c_title = self.extract_title(item_splitted)
            self.show_debug_message("extracted title:")
            self.show_debug_message(c_title)
            self.show_debug_message(c_title[2:4])
            self.show_debug_message(c_title[2:2])

            # BHZ Specials
            # Date as title for Calendars like "Bibel fuer heute"
            if self.checkBoxDaisyDateCalendar.isChecked():
                if c_title[2:4] == "00":
                    # Month as title
                    c_title_date = (
                        c_title[0:2] + " - "
                        + self.comboBoxCopyBhzAusg.currentText()[0:4])
                    self.show_debug_message("title_date:")
                    self.show_debug_message(c_title_date)
                else:
                    if re.match("\d{4,}", c_title) is not None:
                        # Date as title
                        c_title_date = (
                            c_title[2:4] + "." + c_title[0:2] + "."
                            + self.comboBoxCopyBhzAusg.currentText()[0:4])
                    else:
                        # Title unchanged
                        c_title_date = c_title

            # Levels
            if self.checkBoxDaisyLevel.isChecked():
                # multible levels,
                # extract level-no from digit in filename
                # (1. digit after underline)
                self.show_debug_message(item[5:6])
                if self.checkBoxDaisyDateCalendar.isChecked():
                    f_out_file.write(
                        '<h' + item[5:6] + ' id="cnt_'
                        + str(n_counter).zfill(4)
                        + '"><a href="' + str(n_counter).zfill(4)
                        + '.smil#txt_' + str(n_counter).zfill(4)
                        + '">' + c_title_date
                        + '</a></h' + item[5:6] + '>' + '\r\n')
                else:
                    f_out_file.write(
                        '<h' + item[5:6] + ' id="cnt_'
                        + str(n_counter).zfill(4)
                        + '"><a href="' + str(n_counter).zfill(4)
                        + '.smil#txt_' + str(n_counter).zfill(4)
                        + '">' + c_title
                        + '</a></h' + item[5:6] + '>' + '\r\n')
            else:
                if self.checkBoxDaisyDateCalendar.isChecked():
                    f_out_file.write(
                        '<h1 id="cnt_' + str(n_counter).zfill(4)
                        + '"><a href="' + str(n_counter).zfill(4)
                        + '.smil#txt_' + str(n_counter).zfill(4)
                        + '">' + c_title_date + '</a></h1>' + '\r\n')
                else:
                    f_out_file.write(
                        '<h1 id="cnt_' + str(n_counter).zfill(4)
                        + '"><a href="' + str(n_counter).zfill(4)
                        + '.smil#txt_' + str(n_counter).zfill(4)
                        + '">' + c_title + '</a></h1>' + '\r\n')

        f_out_file.write("</body>" + '\r\n')
        f_out_file.write("</html>" + '\r\n')
        f_out_file.close
        self.textEditDaisy.append("<b>NCC-Datei geschrieben</b>")

    def write_master_smil(self, cTotalTime, dir_audios):
        """write MasterSmil-page"""
        try:
            f_out_file = open(
                os.path.join(
                    str(self.lineEditDaisySource.text()), "master.smil"), 'w')
        except IOError as (errno, strerror):
            self.show_debug_message(
                "I/O error({0}): {1}".format(errno, strerror))
            return

        self.textEditDaisy.append(u"<b>MasterSmil-Datei schreiben...</b>")
        f_out_file.write('<?xml version="1.0" encoding="utf-8"?>' + '\r\n')
        f_out_file.write(
            '<!DOCTYPE smil PUBLIC "-//W3C//DTD SMIL 1.0//EN"'
            + ' "http://www.w3.org/TR/REC-smil/SMIL10.dtd">' + '\r\n')
        f_out_file.write('<smil>' + '\r\n')
        f_out_file.write('<head>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:format" content="Daisy 2.02"/>' + '\r\n')
        f_out_file.write('<meta name="dc:identifier" content="'
                         + self.lineEditMetaRefOrig.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="dc:title" content="'
                         + self.lineEditMetaTitle.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:generator"'
                         + ' content="KOM-IN-DaisyCreator"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:format" content="Daisy 2.0"/>'
                         + '\r\n')
        f_out_file.write('<meta name="ncc:timeInThisSmil" content="'
                         + cTotalTime + '" />' + '\r\n')

        f_out_file.write('<layout>' + '\r\n')
        f_out_file.write('<region id="txt-view" />' + '\r\n')
        f_out_file.write('</layout>' + '\r\n')
        f_out_file.write('</head>' + '\r\n')
        f_out_file.write('<body>' + '\r\n')

        z = 0
        for item in dir_audios:
            z += 1
            # splitting
            itemSplit = self.split_filename(item)
            cTitle = self.extract_title(itemSplit)
            f_out_file.write(
                '<ref src="' + str(z).zfill(4) + '.smil" title="'
                + cTitle + '" id="smil_' + str(z).zfill(4) + '"/>' + '\r\n')

        f_out_file.write('</body>' + '\r\n')
        f_out_file.write('</smil>' + '\r\n')
        f_out_file.close
        self.textEditDaisy.append(u"<b>Master-smil-Datei geschrieben</b>")

    def write_smil(self, lTotalElapsedTime, lFileTime, dir_audios):
        """write Smil-Pages"""
        self.textEditDaisy.append(u"<b>smil-Dateien schreiben...</b> ")
        z = 0
        for item in dir_audios:
            z += 1

            try:
                filename = str(z).zfill(4) + '.smil'
                f_out_file = open(os.path.join(
                    str(self.lineEditDaisySource.text()), filename), 'w')
            except IOError as (errno, strerror):
                self.show_debug_message(
                    "I/O error({0}): {1}".format(errno, strerror))
                return
            # else:
            self.textEditDaisy.append(
                str(z).zfill(4) + u".smil - File schreiben")
            # splitting
            itemSplit = self.split_filename(item)
            cTitle = self.extract_title(itemSplit)

            f_out_file.write('<?xml version="1.0" encoding="utf-8"?>' + '\r\n')
            f_out_file.write(
                '<!DOCTYPE smil PUBLIC "-//W3C//DTD SMIL 1.0//EN"'
                + ' "http://www.w3.org/TR/REC-smil/SMIL10.dtd">' + '\r\n')
            f_out_file.write('<smil>' + '\r\n')
            f_out_file.write('<head>' + '\r\n')
            f_out_file.write(
                '<meta name="ncc:generator"'
                + ' content="KOM-IN-DaisyCreator"/>' + '\r\n')
            totalElapsedTime = timedelta(seconds=lTotalElapsedTime[z - 1])
            c_splitted_total_elapsed_time = str(totalElapsedTime).split(".")
            self.show_debug_message("c_splitted_total_elapsed_time: ")
            self.show_debug_message(c_splitted_total_elapsed_time)
            totalElapsedTimehhmmss = c_splitted_total_elapsed_time[0].zfill(8)
            if z == 1:
                # thirst item results in only one split
                totalElapsedTimeMilliMicro = "000"
            else:
                totalElapsedTimeMilliMicro = c_splitted_total_elapsed_time[1][0:3]
            f_out_file.write('<meta name="ncc:totalElapsedTime" content="'
                           + totalElapsedTimehhmmss + "."
                           + totalElapsedTimeMilliMicro + '"/>' + '\r\n')

            fileTime = timedelta(seconds=lFileTime[z - 1])
            self.show_debug_message(u"filetime: " + str(fileTime))
            splittedFileTime = str(fileTime).split(".")
            FileTimehhmmss = splittedFileTime[0].zfill(8)
            # it's only one item in list when no milliseconds
            if len(splittedFileTime) > 1:
                if len(splittedFileTime[1]) >= 3:
                    fileTimeMilliMicro = splittedFileTime[1][0:3]
                elif len(splittedFileTime[1]) == 2:
                    fileTimeMilliMicro = splittedFileTime[1][0:2]
            else:
                fileTimeMilliMicro = "000"

            f_out_file.write(
                '<meta name="ncc:timeInThisSmil" content="'
                + FileTimehhmmss + "." + fileTimeMilliMicro + '" />' + '\r\n')
            f_out_file.write('<meta name="dc:format"'
                           + ' content="Daisy 2.02"/>' + '\r\n')
            f_out_file.write('<meta name="dc:identifier" content="'
                           + self.lineEditMetaRefOrig.text() + '"/>' + '\r\n')
            f_out_file.write('<meta name="dc:title" content="' + cTitle
                           + '"/>' + '\r\n')
            f_out_file.write('<layout>' + '\r\n')
            f_out_file.write('<region id="txt-view"/>' + '\r\n')
            f_out_file.write('</layout>' + '\r\n')
            f_out_file.write('</head>' + '\r\n')
            f_out_file.write('<body>' + '\r\n')
            lFileTimeSeconds = str(lFileTime[z - 1]).split(".")

            f_out_file.write('<seq dur="' + lFileTimeSeconds[0] + '.'
                           + fileTimeMilliMicro + 's">' + '\r\n')
            f_out_file.write('<par endsync="last">' + '\r\n')
            f_out_file.write('<text src="ncc.html#cnt_' + str(z).zfill(4)
                           + '" id="txt_' + str(z).zfill(4) + '" />' + '\r\n')
            f_out_file.write('<seq>' + '\r\n')
            if fileTime < timedelta(seconds=45):
                f_out_file.write('<audio src="' + item
                               + '" clip-begin="npt=0.000s" clip-end="npt='
                               + lFileTimeSeconds[0] + '.' + fileTimeMilliMicro
                               + 's" id="a_' + str(z).zfill(4)
                               + '" />' + '\r\n')
            else:
                f_out_file.write(
                    '<audio src="' + item
                    + '" clip-begin="npt=0.000s" clip-end="npt='
                    + str(15) + '.' + fileTimeMilliMicro + 's" id="a_'
                    + str(z).zfill(4) + '" />' + '\r\n')
                zz = z + 1
                phraseSeconds = 15
                while phraseSeconds <= lFileTime[z - 1] - 15:
                    f_out_file.write(
                        '<audio src="' + item
                        + '" clip-begin="npt=' + str(phraseSeconds) + '.'
                        + fileTimeMilliMicro + 's" clip-end="npt='
                        + str(phraseSeconds + 15) + '.' + fileTimeMilliMicro
                        + 's" id="a_' + str(zz).zfill(4) + '" />' + '\r\n')
                    phraseSeconds += 15
                    zz += 1
                f_out_file.write(
                    '<audio src="' + item
                    + '" clip-begin="npt=' + str(phraseSeconds) + '.'
                    + fileTimeMilliMicro + 's" clip-end="npt='
                    + lFileTimeSeconds[0] + '.' + fileTimeMilliMicro
                    + 's" id="a_' + str(zz).zfill(4) + '" />' + '\r\n')

            f_out_file.write('</seq>' + '\r\n')
            f_out_file.write('</par>' + '\r\n')
            f_out_file.write('</seq>' + '\r\n')

            f_out_file.write('</body>' + '\r\n')
            f_out_file.write('</smil>' + '\r\n')
            f_out_file.close
        self.textEditDaisy.append("<b>smil-Dateien geschrieben:</b> " + str(z))

    def write_config_file(self):
        """write config file"""
        try:
            f_out_file = open("daisy_creator_mag.config", 'w')
        except IOError as (errno, strerror):
            self.show_debug_message(
                "I/O error({0}): {1}".format(errno, strerror))
            return

        f_out_file.write('[Ordner]' + '\n')
        f_out_file.write(
            'BHZ=' + self.lineEditConfigBHZ.text() + '\n')
        f_out_file.write(
            'BHZ-Ziel=' + self.lineEditConfigBHZDest.text() + '\n')
        f_out_file.write(
            'BHZ-Meta=' + self.lineEditConfigBHZMeta.text() + '\n')
        f_out_file.write(
            'BHZ-Ausgabeansage=' + self.lineEditConfigBHZIssueNr.text() + '\n')
        f_out_file.write(
            'BHZ-Intro=' + self.lineEditConfigBHZIntro.text() + '\n')
        f_out_file.write(
            'BHZ-Zusatzdatei=' + self.lineEditConfigBHZFileAdditionally.text()
            + '\n')
        f_out_file.write('\n')
        f_out_file.write('[Dateien]' + '\n')
        f_out_file.write(
            'BHZ-Ausgabeansage-Praefix='
            + self.lineEditConfigBHZPrefixIssueNr.text() + '\n')
        f_out_file.write(
            'BHZ-Meta-Praefix='
            + self.lineEditConfigBHZPrefixMeta.text() + '\n')
        f_out_file.write('\n')
        f_out_file.write('[Blindenhoerzeitschriften]' + '\n')
        f_out_file.write('BHZ=' + self.lineEditConfigBHZDesc.text() + '\n')
        f_out_file.write('\n')
        f_out_file.write('[Programme]' + '\n')
        f_out_file.write('LAME=' + self.lineEditConfigLame.text())
        f_out_file.write('\n')
        f_out_file.close
        self.show_debug_message("write config file: daisy_creator_mag.config")
        self.textEditConfig.append("<b>Neue Config-Datei geschrieben:</b>")
        self.textEditConfig.append("daisy_creator_mag.config")
        self.progressBarConfig.setValue(20)

    def write_meta_file(self, path_file_source):
        """write config meta file"""
        try:
            f_out_file = open(path_file_source, 'w')
        except IOError as (errno, strerror):
            self.show_debug_message(
                "I/O error({0}): {1}".format(errno, strerror))
            return

        title_year = (self.lineEditMetaTitle.text()
                      [0:len(self.lineEditMetaTitle.text()) - 5]
                      + self.lineEditConfigBHZMetaYear.text() + "-")

        f_out_file.write('[Daisy_Meta]' + '\n')
        f_out_file.write(
            'Produzent=' + self.lineEditMetaProducer.text() + '\n')
        f_out_file.write(
            'Autor=' + self.lineEditMetaAutor.text() + '\n')
        f_out_file.write(
            'Titel=' + title_year + '\n')
        f_out_file.write(
            'Edition=' + self.lineEditMetaEdition.text() + '\n')
        f_out_file.write(
            'Sprecher=' + self.lineEditMetaNarrator.text() + '\n')
        f_out_file.write(
            'Stichworte=' + self.lineEditMetaKeywords.text()
            + '\n')
        f_out_file.write(
            'ISBN/Ref-Nr.Original='
            + self.lineEditMetaRefOrig.text() + '\n')
        f_out_file.write('Verlag=' + self.lineEditMetaPublisher.text() + '\n')
        f_out_file.write('Jahr=' + self.lineEditConfigBHZMetaYear.text())
        f_out_file.write('\n')
        f_out_file.close
        self.show_debug_message("write meta file: " + path_file_source)

    def split_filename(self, item):
        """split filename into list"""
        if self.comboBoxDaisyTrenner.currentText() == "Ausgabe-Nr.":
            itemSplit = item.split(self.comboBoxCopyBhzAusg.currentText()
                                   + "_")
            # itemSplit = item.split("_", 2)
        self.show_debug_message(itemSplit)
        self.show_debug_message(len(itemSplit))
        return itemSplit

    def extract_title(self, itemSplit):
        """extract title """
        # last piece
        itemLeft = itemSplit[len(itemSplit) - 1]
        # now split file-extention
        itemTitle = itemLeft.split(".mp3")
        cTitle = re.sub("_", " ", itemTitle[0])
        return cTitle

    def show_dialog_critical(self, error_message):
        """show messagebox warning"""
        QtGui.QMessageBox.critical(self, "Achtung", error_message)

    def show_dialog_question(self, display_message):
        """show messagebox question"""
        msgBox = QtGui.QMessageBox()
        msgBox.setIcon(QtGui.QMessageBox.Question)
        msgBox.setInformativeText(display_message)
        msgBox.setText("Achtung!")
        msgBox.setStandardButtons(
            QtGui.QMessageBox.Save | QtGui.QMessageBox.Cancel)
        msgBox.setDefaultButton(QtGui.QMessageBox.Save)
        ret = msgBox.exec_()
        action = 0
        if ret == QtGui.QMessageBox.Save:
            # Save was clicked
            debugMessage = "Save was clicked"
            self.show_debug_message(debugMessage)
            action = 1
        elif ret == QtGui.QMessageBox.Cancel:
            # cancel was clicked
            debugMessage = "Cancel was clicked"
            self.show_debug_message(debugMessage)
        return action

    def show_debug_message(self, debugMessage):
        """show messagebox """
        if self.app_debug_mod == "yes":
            print debugMessage

    def action_quit(self):
        """exit the application"""
        QtGui.qApp.quit()

    def make_config_dirs(self):
        """make config dirs"""
        self.show_debug_message("make config dirs:")
        self.textEditConfig.append("<b>Neue Config-Ordner:</b>")
        config_dirs = []
        config_dirs.append(self.lineEditConfigBHZ.text())
        config_dirs.append(self.lineEditConfigBHZDest.text())
        config_dirs.append(self.lineEditConfigBHZMeta.text())
        config_dirs.append(self.lineEditConfigBHZIssueNr.text())
        config_dirs.append(self.lineEditConfigBHZIntro.text())
        n = 0
        for config_dir in config_dirs:
            try:
                os.makedirs(str(config_dir))
                self.show_debug_message(config_dir)
            except OSError:
                if not os.path.isdir(config_dir):
                    self.show_dialog_critical(
                        "Fehler beim erstellen von: " + config_dir)
                else:
                    self.show_debug_message("dir always exists:")
                    self.show_debug_message(config_dir)
            self.textEditConfig.append(config_dir)
            n += 1
        self.progressBarConfig.setValue(30)

    def main(self):
        """mainfunction"""
        self.show_debug_message("let's rock")
        self.read_config()
        self.check_packages(self.app_lame)
        self.progressBarCopy.setValue(0)
        self.progressBarDaisy.setValue(0)
        self.progressBarConfig.setValue(0)
        # Bhz in Combo
        for item in self.app_bhz_items:
            self.comboBoxCopyBhz.addItem(item)
        # Combo-items: numbers according to years
        prevYear = str(datetime.datetime.now().year - 1)
        currentYear = str(datetime.datetime.now().year)
        nextYear = str(datetime.datetime.now().year + 1)
        for item in self.app_issue_items_prev:
            self.comboBoxCopyBhzAusg.addItem(prevYear + "_" + item)
        for item in self.app_issue_items_current:
            self.comboBoxCopyBhzAusg.addItem(currentYear + "_" + item)
        for item in self.app_issue_items_next:
            self.comboBoxCopyBhzAusg.addItem(nextYear + "_" + item)
        # Combo-items: string for splitting author and title in filenames
        self.comboBoxDaisyTrenner.addItem("Ausgabe-Nr.")
        self.comboBoxDaisyTrenner.addItem(prevYear)
        self.comboBoxDaisyTrenner.addItem(currentYear)
        self.comboBoxDaisyTrenner.addItem(nextYear)
        self.comboBoxDaisyTrenner.addItem("-")
        self.comboBoxDaisyTrenner.addItem("_")
        self.comboBoxDaisyTrenner.addItem("_-_")
        self.comboBoxPrefBitrate.addItem("64")
        self.comboBoxPrefBitrate.addItem("80")
        self.comboBoxPrefBitrate.addItem("96")
        self.comboBoxPrefBitrate.addItem("128")
        # defaults for checkboxes
        self.checkBoxCopyBhzIntro.setChecked(True)
        self.checkBoxCopyBhzAusgAnsage.setChecked(True)
        self.checkBoxCopyID3Change.setChecked(True)
        self.checkBoxCopyBitrateChange.setChecked(True)
        self.checkBoxCopyFileNameShort.setChecked(True)
        # Help-Text
        self.read_help()
        self.read_user_help()
        self.show()


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    dyc = DaisyCopy()
    dyc.main()
    app.exec_()
    # This shows the interface we just created. No logic has been added, yet.
