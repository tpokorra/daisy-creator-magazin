daisy-creator-magazin
=====================

Create digital talking books of audio magazins for blind people in the daisy 2.02 standard.

Basics
------
The workflow is as follows:
- Record your articles with a digital recorder, like the marantz PMD 661
- The produced audiofiles are stored as mp3 in a flat struckture with numbered filenames up to 1000
- Mount the recordet media-card to your PC or copy the files to the PC
- Now start the daisy-creator-magazin-application to create the daisy-fileset

If finished, you will find in the destinationfolder your audiofiles
and the daisy-structure for your daisy-player.
The audiofiles are renamend like your metadata.

General work steps
------------------
1. Choose the folder of your recordet audiofiles as "Source-Folder"
2. Choose the folder for your dtb as "Destination-Folder"
3. Choose the title and issue number of your magazin
4. Select the options if necessary (see options-section)
5. Run "Copy"
6. Select tab two
7. Control and edit your metadata if necessary
8. Select tab three
9. Edit options if necessary
10. Run "Daisy"
Done!

Additional audiofiles-section
-----------------------------
With this, you can load and insert in the daisystruckture one or two separate recordet audiofiles,
they not are on your mediacard. To sort the audiofiles before your audios from the mediacard,
lets the filename begin with the number "0101".

Options-Section on tab one
--------------------------
Here, you can select:
1. Copy Magazin-Issue
This will copy a audiofile, with e.g. a separate produced announcement of your issue-number, in the daisy-structure
2. Copy Intro
This will copy a audiofile with a separate produced jingle or intro
3. Change bitrate
This checks the bitrate of each audiofile and changes it to the choosen rate on the "Preferences-Tab"
4. Rename 1000 (or 1001) to 0100 (0101)
This is used, when the first recordet audiofile (e.g. with a special announcement)
must sorted before the additive audiofiles (see Additional audiofiles)
5. For DTB TOC: Use titles from filenames and cut leading numbers in filename for TOC
6. Read levels from filenames (digit after first underscore: 1001_2)
7. Read date from filenames for DTB TOC
8. Chose character for split author and title for DTB TOC
9. Bitrate

Preferences
-----------
- The titles and available numbers of your magazins will be load from the configfile: daisy_creator_mag.config.
See daisy_creator_mag.config.sample for the syntax.
- The metadata for each magazin will be load from a file in the folder (set in config-file) by the name of the magazin.
See Daisy_Meta_My_Magazin.sample for the syntax

Config
------

On the config tab you can

* for example edit your working paths
* and, if you organaize your files in folders, one for every year,
  make the folder struckture for the new year

To create a new config edit the entrys for your needs:

* The first entrys contains the working paths and folders
* The "Jahr-Metadata" is for the year, that is predefined in the meta files of each Magazin
* The Präfix items contains the prefixes for mata and issue nr files
* BHZ-Liste defines the items of magazins, listet in the drop downbox on tab 1
* Lame must contain the path to the lame bin

If are all settings done, press "Speichern", the following tasks will be done:

* The old config file is backed up
* New working paths and folders are created
* Intro files are copied form old to new destination
* Meta files are created from old ones and updatet to the new year

Remark
------
The daisy_creator_mag is written by Joerg Sorge for KOM-IN-Netzwerk e.V. www.kom-in.de

Credits
-------
Icons from https://icons8.com

Installation
------------
- Install dependencys

  * `sudo apt install qt4-default python-qt4 python-mutagen`

- Clone the repo `git clone https://gitlab.com/tigexplorer/daisy-creator-magazin.git`
- Install the Desktop-Launcher

  * Copy the file `daisy_creator_mag_py.desktop.sample` to `daisy_creator_mag_py.desktop`
  * Edit the paths in the file for your needs
  * Copy the file to `/usr/share/applications/`
  * To add DaisyCreator to the favorities under Gnome 3
    Launch the `Activities`, right-Klick on the DaisyCreator Icon, choose `Add to Favorities`
